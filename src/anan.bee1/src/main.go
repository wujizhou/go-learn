package src

import (
	_ "anan.bee1/routers"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)

func init() {
	cfg := beego.AppConfig
	orm.RegisterDriver(
		"mysql",
		orm.DRMySQL)
	orm.RegisterDataBase(
		"default",
		"mysql",
		cfg.String("mysqluser")+":"+cfg.String("mysqlpass")+"@/"+cfg.String(
			"mysqlhost")+"?charset=utf8")
}
func main() {
	//bee generate scaffold user -fields="id:int64,name:string,nick:string,pass:string,avatar:string,status:int8,sex:uint8" -driver=mysql -conn="root:1@tcp(127.0.0.1:3306)/ebuy_2613747851"
	// static
	beego.SetStaticPath("/css", "static/css")
	beego.SetStaticPath("/img", "static/img")
	beego.SetStaticPath("/js", "static/js")
	beego.Run()
}

