package controller

import (
	"src/logic"
	"github.com/kataras/iris"
	"github.com/kataras/iris/mvc"
	"src/model"
	"github.com/gpmgo/gopm/modules/log"
	"time"
)

type IndexController struct {
	Ctx iris.Context
	Logic logic.StarInfoLogic
}

func (c *IndexController) Get() mvc.Result {
	list := c.Logic.GetAll()
	return mvc.View{
		Name : "index/index.tpl",
		Data : iris.Map{
			"Title":"球星库",
			"List": list,
		},
		Layout:"tpl/base.tpl",
	}
}

func (c *IndexController) GetSet(id int) mvc.Result{
	id,err := c.Ctx.URLParamInt("id")
	var info *model.StarInfo
	if err == nil {
		info = c.Logic.Get(id)
	}
	return mvc.View{
		Name:"index/set.tpl",
		Data: iris.Map{
			"Title" : "球星库",
			"Info" : info,
		},
		Layout:"tpl/base.tpl",
	}
}

func (c *IndexController) PostSet() mvc.Result {
	info := model.StarInfo{}
	err := c.Ctx.ReadForm(&info)
	if err != nil {
		log.Fatal(err)
	}
	if info.Id >0 { //upd
		info.SysUpdated = int(time.Now().Unix())
		c.Logic.Upd(&info,[]string{"name_zh","name_en","avatar","birthday","height","weight",
		"club","jersy","country","moreinfo"})
	}else{ //add
		info.SysCreated = int(time.Now().Unix())
		c.Logic.Add(&info)
	}
	return mvc.Response{
		Path:"/admin",
	}
}

func (c *IndexController) GetDel() mvc.Result {
	id,err := c.Ctx.URLParamInt("id")
	if err == nil {
		c.Logic.Del(id)
	}
	return mvc.Response{
		Path:"/admin",
	}
}