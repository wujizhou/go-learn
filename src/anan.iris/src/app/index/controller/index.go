package controller

import (
	"src/logic"
	"github.com/kataras/iris"
	"github.com/kataras/iris/mvc"
)

type IndexController struct {
	Ctx iris.Context
	Logic logic.StarInfoLogic
}

func (c *IndexController) Get() mvc.Result {
	list := c.Logic.GetAll()
	return mvc.View{
		Name : "index/index.tpl",
		Data : iris.Map{
			"Title":"球星库",
			"List": list,
		},
	}
}

func (c *IndexController) GetBy(id int) mvc.Result{
	if id<1 {
		return mvc.Response{
			Path: "/",
		}
	}
	info := c.Logic.Get(id)
	return mvc.View{
		Name:"index/info.tpl",
		Data: iris.Map{
			"Title" : "球星库",
			"Info" : info,
		},
	}
}

func (c *IndexController) GetSearch() mvc.Result {
	country := c.Ctx.URLParam("country")
	list := c.Logic.Search(country)
	return mvc.View{
		Name : "index/index.tpl",
		Data : iris.Map{
			"Title":"球星库",
			"List": list,
		},
	}
}