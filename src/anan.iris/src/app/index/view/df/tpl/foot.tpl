{{ define "tpl/foot" }}
  {{/*stat code/js/..*/}}
        <footer style="text-align: center;">
            <p>&copy; 2018 - {{.AppOwner}}</p>
        </footer>
    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//cdn.bootcss.com/jquery/3.4.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="//cdn.bootcss.com/jquery/3.4.1/jquery.min.js"><\/script>')</script>
    <script src="//cdn.bootcss.com/twitter-bootstrap/3.4.1/js/bootstrap.min.js"></script>
</body>
</html>
{{ end }}