package dao

import (
	"github.com/go-xorm/xorm"
	"model"
	"log"
)

type SuperStarDao struct {
	engine *xorm.Engine
}

func NewSuperStarDao(engine *xorm.Engine) *SuperStarDao {
	return &SuperStarDao{ engine:engine, }
}

func (d *SuperStarDao) Get(id int) *model.StarInfo {
	info := &model.StarInfo{Id:id}
	ok,err := d.engine.Get(info)
	if ok && err == nil {
		return info
	}else{
		// return nil
		info.Id = 0
		return info
	}
	return nil
}
func (d *SuperStarDao) GetAll() []model.StarInfo {
	// list := make([]model.StarInfo,0)
	list := []model.StarInfo{}
	err := d.engine.Desc("id").Find(&list)
	if err != nil {
		log.Println(err)
		return list
		// return nil
	}
	return list
}

func (d *SuperStarDao) Search(country string) []model.StarInfo {
	list := []model.StarInfo{}
	err := d.engine.Where("countru=?",country).
		Desc("id").Find(&list)
	if err != nil {
		log.Println(err)
		return list
	}
	return list
}

func (d *SuperStarDao) Del(id int) error{
	data := &model.StarInfo{Id:id,SysStatus:1}
	_,err := d.engine.Id(data.Id).Update(data)
	return err
}

func (d *SuperStarDao) Upd(data *model.StarInfo,cols []string) error{
	_,err := d.engine.Id(data.Id).MustCols(cols...).Update(data)
	return err
}

func (d *SuperStarDao) Add(data *model.StarInfo) error{
	_,err := d.engine.Insert(data)
	return err
}
