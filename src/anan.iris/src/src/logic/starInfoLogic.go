package logic

import (
	"model"
	"dao"
	"helper"
)

type StarInfoLogic interface {
	GetAll() []model.StarInfo
	Get(id int) *model.StarInfo
	Del(id int) error
	Upd(user *model.StarInfo,cols []string) error
	Add(user *model.StarInfo) error
	Search(country string) []model.StarInfo
}

type starInfoLogic struct {
	dao *dao.SuperStarDao
}

func NewSuperStarLogic() StarInfoLogic {
	return &starInfoLogic{
		dao: dao.NewSuperStarDao(helper.InstanceMaster()),
	}
}
func (s *starInfoLogic) GetAll() []model.StarInfo {
	return s.dao.GetAll()
}
func (s *starInfoLogic) Get(id int) *model.StarInfo{
	return s.dao.Get(id)
}
func (s *starInfoLogic) Del(id int) error{
	return s.dao.Del(id)
}
func (s *starInfoLogic) Upd(user *model.StarInfo,cols []string) error{
	return s.dao.Upd(user,cols)
}
func (s *starInfoLogic) Add(user *model.StarInfo) error{
	return s.dao.Add(user)
}

func (s *starInfoLogic) Search(country string) []model.StarInfo{
	return s.dao.Search(country)
}