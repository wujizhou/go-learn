package middle

import "github.com/kataras/iris/middleware/basicauth"

var BaseAuth = basicauth.New(basicauth.Config{
	Users: map[string]string{
		"admin":"pass",
	},
})