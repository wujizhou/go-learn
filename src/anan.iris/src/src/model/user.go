package model

type User struct {
	Id     int    `xorm:"not null pk autoincr INT(11)"`
	Name   string `xorm:"not null comment('系统用户名,df:u时间戳?') unique VARCHAR(32)"`
	Nick   string `xorm:"not null comment('昵称,显示,df:n时间戳 ?') VARCHAR(32)"`
	Pass   string `xorm:"not null comment('密码,salt加密') VARCHAR(96)"`
	Avatar string `xorm:"not null comment('头像, picId / 微信http') VARCHAR(255)"`
	Status int    `xorm:"not null default 0 TINYINT(2)"`
	Sex    int    `xorm:"not null default 0 comment('ENUM:0=>默认,1=>男,2=>女') TINYINT(2)"`
}
