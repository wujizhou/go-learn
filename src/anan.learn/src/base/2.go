// data type
package base

import (
	"fmt"
	"unsafe"
	"reflect"
)
type rb int32
//var u uint8
//var b bool = true
func main() {
	var i int
	var b byte = 1 // uint8
	//var r rune = 1 // int32
	var rr rb = 33
	//var i int = 1 // base os
	//var f float32 = 1.1
	//var b bool = true
	//var c complex64
	var s string
	// df : 0/false/""
	fmt.Println(unsafe.Sizeof(i))
	fmt.Sprintf("\n%s%s%s",s,rr,b)
	var ff = int(rr)
	fmt.Print(reflect.TypeOf(ff))
}
