package base

import (
	"fmt"
	"reflect"
	"learn/import"
)

var a,b,c int = 1,3,3
var (
	aa string = "test是"
	bb int
)
const c1 = "23" + "3"
const c2,c4 string = "23","23"
const(
	c3 string = "2手动"
)
const aalen = len(c3) // can be var
var As = 2 // can import
func main() {
	d,_ := 1,2 // only in func
	e := int32(a)
	fmt.Print(e)
	//reflect.TypeOf(c)
	//fmt.Print(aa)
	//fmt.Print(bb)
	fmt.Print(reflect.TypeOf(d))
	var f float32 = 3.9
	fmt.Print(int64(f))        //3
	fmt.Print(_import.As)      //3
	fmt.Println()              //3
	fmt.Println(string(aa[2])) //3
	fmt.Println(aalen)         //3
	//fmt.Println(strings.Index(aa,"t"))
}
