package extends

import "fmt"

// 抽象类 ..........
type AbstructClass struct {
	// "子类"给此字段赋值
	beforeTask func()
	// "子类"给此字段赋值
	afterTask func()
}

func (ac *AbstructClass) inTask() {
	fmt.Println("ac in ")
}

// 调用所有任务步骤
func (ac *AbstructClass) RunTask() {
	ac.beforeTask()
	ac.inTask()
	ac.afterTask()
}