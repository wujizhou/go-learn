package extends

import (
	"fmt"
)
// 实现类 ......
type Class struct {
	// "继承"模板类
	AbstructClass
}

// 实现模板中的beforeTask方法
func (c *Class) beforeTask() {
	fmt.Println("c before")
}
// 实现模板中的afterTask方法
func (c *Class) afterTask() {
	fmt.Println("c after")
}
// 构造一个Class结构体
func NewClass() *Class {
	c := new(Class)
	// 重写到子类逻辑为父类
	// c.AbstructClass.beforeTask = c.beforeTask
	// c.AbstructClass.afterTask  = c.afterTask

	return c
}