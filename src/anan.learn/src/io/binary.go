package main

import (
	"os"
	"encoding/binary"
	"fmt"
)

type BitmapInfoHeader struct {
	Size uint32
	Width int32
	Height int32
	Places uint16
	BitCount uint16
	Compression uint16
	SizeImage uint32
	XpixsPreMeter int32
	YpixsPreMeter int32
	ClsrUsed uint32
	ClrImportant uint32
}

func main() {
	file,_ := os.Open("img.bmp")
	defer file.Close()
	// 1.
	var headA,headB byte
	binary.Read(file,binary.LittleEndian,&headA)
	binary.Read(file,binary.LittleEndian,&headB)
	fmt.Printf("%c%c",headA,headB)

	//var size uint32
	//binary.Read(file,binary.LittleEndian,&size)
	//var reservedA,reservedB uint16
	//binary.Read(file,binary.LittleEndian,&reservedA)
	//binary.Read(file,binary.LittleEndian,&reservedB)
	//
	//var offbits uint32
	//binary.Read(file,binary.LittleEndian,&offbits)
	//fmt.Println(headA,size)

	// 2.
	infoHeader := new(BitmapInfoHeader)
	binary.Read(file,binary.LittleEndian,infoHeader)
	fmt.Println(infoHeader)
}
