package main

import (
	"os"
	"fmt"
	"bufio"
)

func main() {
	if len(os.Args)<2 {
		return
	}
	filename := os.Args[1]
	file,err := os.Open(filename)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()
	var c int
	reader := bufio.NewReader(file)
	for {
		_,isPrefix,err := reader.ReadLine()
		if err != nil {
			break
		}
		if !isPrefix {
			c++
		}
	}
	fmt.Println(c)
}
