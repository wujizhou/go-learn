package main

import (
	"strings"
	"bufio"
	"fmt"
	"os"
)

func main() {
	strReader := strings.NewReader("hello world")

	bufReader := bufio.NewReader(strReader)
	data,_ := bufReader.Peek(5) // copy
	fmt.Println(data,bufReader.Buffered())

	str,_ := bufReader.ReadString(' ')
	fmt.Println(str,bufReader.Buffered()) //read and slice

	w := bufio.NewWriter( os.Stdout)
	fmt.Fprint(w,"hello ")
	fmt.Fprint(w,"world!")
	w.Flush()
}
