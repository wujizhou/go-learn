package main

import (
	// "anan.learn/src/test"
	// "net"
	"anan.learn/src/extends"
)

// import . "strings" //本地化导入,不写前缀
// import _ "strings" // 仅初始化代码包 => init()
// import f "flag"    //别名

//func test() {
//
//}

func main() {
	// a := 1
	// p := &a
	// fmt.Print(p)
	
	// net.Listen()
	//
	class := extends.NewClass()
	class.RunTask()
	//
	// m := test.New("rb",0,0)
	// m.Hi("hi")
	
  //var numbers2 [5]int
	//numbers2[0] = '2'
	//numbers2[3] = numbers2[0] - 3 //-1
	//numbers2[1] = numbers2[2] + 5 // 5
	//numbers2[4] = len(numbers2)   //5

	//a := []int{0,1,2,3,4,5}
	//fmt.Println(a[:3])
	//fmt.Println(cap(a))

	//var numbers4 = [...]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	//for k,v := range numbers4 {
	//	fmt.Println(k,v)
	//}

	// fmt.Print(time.Now().Unix())

	// time.ParseInLocation()
	// mm2 := map[string]int{"golang": 42, "java": 1, "python": 8}
	// mm2["scala"]  = 25
	// mm2["erlang"] = 50
	// mm2["python"] = 0
	// fmt.Printf("%d, %d, %d \n", mm2["scala"], mm2["erlang"], mm2["python"])
	//slice5 := numbers4[4:6:8]
	//fmt.Println(cap(slice5))
}