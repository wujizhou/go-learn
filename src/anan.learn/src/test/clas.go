package test

import "fmt"

type clas struct {
	name string
	age int
	sex int
}

func (m *clas) Hi(s string) {
	fmt.Println("clas" + s + m.name + "\n")
}
