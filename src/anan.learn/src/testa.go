package main

import "fmt"

type B struct {
	bField string
}

func (b *B) bMethod() {
	fmt.Print("b.bMethod : ")
}

type A struct {
	B // A可以【直接】访问B里所有字段和方法
}

func main() {
	a := new(A)
	fmt.Print("a.bField : "+a.bField)  // OK
	a.bMethod() // OK
}


