package main

import (
  "github.com/go-martini/martini"
  "net/http"
  "fmt"
)

func main() {
  m := martini.Classic()
  m.Get("/", func() string {
    println("hello world") // console
    return "Hello world!"
  })
  m.Get("/test", func() {
  })
  m.Get("/test2", func() (int, string) {
	  return 418, "i'm a teapot" // HTTP 418 : "i'm a teapot"
	})
  m.Get("/test3", func(res http.ResponseWriter, req *http.Request) { // res 和 req 是通过Martini注入的
    res.WriteHeader(200) // HTTP 200
  })
  m.Get("/hello/:name", func(params martini.Params) string {
    return "Hello " + params["name"]
  })
  m.Get("/hello2/**", func(params martini.Params) string {
    return "Hello2 " + params["_1"]
  })
  m.Get("/hello3/(?P<name>[a-zA-Z]+)", func(params martini.Params) string {
    return fmt.Sprintf ("Hello3 %s", params["name"])
  })
  m.Get("/secret", authorize, func() {
    // 该方法将会在authorize方法没有输出结果的时候执行.
  })
  m.Group("/books2", func(r martini.Router) {
    r.Get("/:id", GetBooks)
    r.Post("/new", NewBook)
    r.Put("/update/:id", UpdateBook)
    r.Delete("/delete/:id", DeleteBook)
  }, MyMiddleware1, MyMiddleware2)
  m.Run()
}
func GetBooks(){

}
func MyMiddleware1() {

}
func MyMiddleware2() {

}
func NewBook(){

}
func UpdateBook(){

}
func DeleteBook(){

}
func authorize() {

}