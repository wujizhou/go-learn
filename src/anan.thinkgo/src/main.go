package main

import (
    "github.com/thinkoner/thinkgo"
    "fmt"
    "github.com/thinkoner/thinkgo/router"
    "github.com/thinkoner/thinkgo/context"
)

func main() {
    app := thinkgo.BootStrap()
    app.RegisterRoute(func(route *router.Route) {

        route.Get("/", func(req *context.Request) *context.Response {
            return thinkgo.Text("Hello ThinkGo !")
        })

        route.Get("/ping", func(req *context.Request) *context.Response {
            return thinkgo.Json(map[string]string{
                "message": "pong",
            })
        })

        // Dependency injection
        route.Get("/user/{name}", func(req *context.Request, name string) *context.Response {
            return thinkgo.Text(fmt.Sprintf("Hello %s !", name))
        })
    })
    // listen and serve on 0.0.0.0:9011
    app.Run()
}